<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cookiechoices_titre' => 'Cookiechoices',
	'cfg_titre_parametrages' => 'Configuration de cookiechoices', 

	// T
	'titre_page_configurer_cookiechoices' => 'Configuration de cookiechoices',

	// M
	'message_message' => 'En poursuivant votre navigation sur ce site, vous  acceptez  l\'utilisation de cookies pour vous proposer des contenus et services adaptés',
	'message_closetext' => 'OK', 
	'message_policytext' => 'En savoir plus',

	// L
	'label_section_close' => 'Bouton d\'accord des cookies', 
	'label_close_button' => 'Afficher le bouton',
	'label_close_txt' => 'Personnaliser le texte du bouton (facultatif)',
	'label_close_txt_current' => 'Intitulé du bouton par défaut',
	'label_section_policy' => 'Bouton de lien vers la politique de confidentialité',  
	'label_policy_button' => 'Afficher le bouton',
	'label_policy_url' => 'Adresse de la page contenant la politique de confidentialités',
	'label_policy_txt' => 'Personnaliser le texte du bouton (facultatif)',
	'label_policy_txt_current' => 'Intitulé du bouton par défaut',
	'label_position' => 'Type',
	'label_section_graphisme' => 'Aspect graphique',
	'label_effet' => 'Disparation de la barre',
	'label_position_popin' => 'Ecran d\'accueil (popin)',
	'label_position_barre' => 'Barre horizontale', 
	'label_section_message' => 'Message',
	'label_message_txt_current' => 'Message par défaut',
	'label_message_txt' => 'Personnaliser le message (facultatif) syntaxe &lt;multi&gt;&lt;/multi&gt; accepté',
);

