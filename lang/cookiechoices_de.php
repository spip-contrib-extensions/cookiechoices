<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cookiechoices_titre' => 'Cookie Auswahl',
	'cfg_titre_parametrages' => 'Konfiguration von Cookiechoices', 

	// T
	'titre_page_configurer_cookiechoices' => 'Konfiguration von Cookiechoices',

	// M
	'message_message' => 'Durch Benutzung dieser WWW-Seiten akzeptieren Sie unsere Einstellungen für Cookies und Logdateien',
	'message_closetext' => 'OK', 
	'message_policytext' => 'Weitere Informationen',

	// L
	'label_section_close' => 'Schaltfläche Cookie-Zustimmung', 
	'label_close_button' => 'Schaltfläche anzeigen',
	'label_close_txt' => 'Text der Schaltfläche anpassen (optional)',
	'label_close_txt_current' => 'Standardbezeichnung der Schaltfläche',
	'label_section_policy' => 'Schaltfläche für den Link zu Datenschutz- und Nutzungsbedingungen',  
	'label_policy_button' => 'Schaltfläche anzeigen',
	'label_policy_url' => 'Adresse Seite mit Informationen zu Cookies und Datenschutzbestimmungen',
	'label_policy_txt' => 'Text der Schaltfläche anpassen (optional)',
	'label_policy_txt_current' => 'Standardbezeichnung der Schaltfläche',
	'label_position' => 'Typ',
	'label_section_graphisme' => 'Grafische Darstellung',
	'label_effet' => 'Verbergen des Banners',
	'label_position_popin' => 'Startbildschirm (Popup/Overlay)',
	'label_position_barre' => 'Horizontales Banner', 
	'label_section_message' => 'Hinweistext',
	'label_message_txt_current' => 'Standard-Hinweistext',
	'label_message_txt' => 'Hinweistext anpassen (optional) die Syntaxe &lt;multi&gt;&lt;/multi&gt; ist möglich.',
);

