<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cookiechoices_description' => 'Zustimmung zur Verwendung von Cookies einholen.',
	'cookiechoices_nom' => 'CookieChoices',
	'cookiechoices_slogan' => 'Zeigt einen Hinweis zur Verwendung von Cookies an und ermöglicht ausdrüciche Zustimmung',
);

